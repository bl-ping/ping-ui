import React, { useState } from 'react';
import './App.css';

function App() {
  const [date, setDate] = useState("");

  function getTime() {
    fetch("https://ping-pong-time.herokuapp.com/api/pingpong")
    // fetch("http://localhost:8080/api/pingpong")
      .then((response) => {
        return response.text();
      })
      .then((data) => {
        setDate(data)
      })
  }


  return (
    <div className="App">
      {console.log("Date: ", date)}
      <header className="Ping Pong">
        <h1>Last Successful Ping: {date}</h1>
        <button onClick={getTime}>Don't Press This Button!</button>
      </header>
    </div>
  );
}

export default App;
